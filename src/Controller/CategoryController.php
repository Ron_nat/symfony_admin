<?php

namespace App\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use FOS\RestBundle\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use App\Entity\Category;
use App\Form\CategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class CategoryController extends Controller
{
    /**
     * @Route("/category/{id}", name="findonecategory",methods={"get"})
     */
    public function index(ObjectManager $em,Request $request,$id)
    {
        $category = $em->getRepository(Category::class)->find($id);
        return $this->json('here');
    }

    /**
     * @Route("/category/create", name="addonecategory", methods={"post"})
     */
    public function Create(Request $req,ValidatorInterface $validator,ObjectManager $em){
        $cat = new Category();
        $errors = [];
        $datas = $req->request->all();
        if(isset($datas['id']) && $datas['id'] != 0){
            $cat->setId($datas['id']);
        }
        $cat->setName($datas['name']);
        $cat->setParent($datas['parent']);
        $error = $validator->validate($cat);
        if($error->count() > 0){
            foreach ($error as $e){
                $errors[] = $e->getMessage();
            }
        } else {
            $em->persist($cat);
            if($em->flush()){
                $errors[] = 'database write faild';
            }
        }
        return $this->json([
            'cat'=>$datas,
            'error'=>$errors
        ]);
    }

    /**
     * @param ObjectManager $em
     * @Route("/category/getall", name="_getall")
     */
    public function getAll(ObjectManager $em){
        $allDatas = $em->getRepository(Category::class)->findAll();
        return $this->json(
            $allDatas
        );
    }

    /**
     * @param ObjectManager $em
     * @Route("/category/getparents",name="_parents")
     */
    public function getParentCategorys(ObjectManager $em,Request $request){
        $allDatas = $em->getRepository(Category::class)->findBy(["parent"=>0]);
        return $this->json(
            [
                'categorys'=>$allDatas
            ]
        );
    }

    /**
     * @param ObjectManager $em
     * @param Request $request
     * @Route("/category/getChilds/{parentId}",name="_childs")
     */
    public function getChilds(ObjectManager $em,$parentId){
        $allDatas = $em->getRepository(Category::class)->findBy(["parent"=>$parentId]);
        return $this->json(
            $allDatas
        );
    }


}
